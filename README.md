# HAProxy

Deploy HA Proxy as an IPv4+IPv6 proxy for Nginx Ingress Controller in Kubernetes.

Monitoring:

* Metricbeat

## Install

```bash
ansible-galaxy install 'git+https://gitlab.com/captnbp/ansible-haproxy-role.git' -f
```

## Requirements

## Role Variables

## Dependencies

##  Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:


```yaml
- hosts: servers
  roles:
    - { role: ansible-haproxy-role, tags: ['haproxy'] }
```

## Example Playbook for Gitlab CI integration

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

In your Gitlab repository, set the following variables in `Settings -> CI / CD -> Variables`

```yaml
- hosts: servers
  roles:
    - { role: ansible-haproxy-role, tags: ['haproxy'] }
```

## License

BSD

## Author Information

Contact me at benoit@doca-consulting.fr
